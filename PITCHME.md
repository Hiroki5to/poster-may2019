### My progress-2019/05/21

M2 Hiroki Goto

---

### DONE LIST

Analyzed Nagasaki experiment data 

Submitted to DICOMO

Held Open Campus

---

### TO DO LIST

Experimenting in Fukuoka

Exhibiting in Tokyo

Exhibiting in UBI conference

Preparing a experiment in Miyazaki

---

### RESEARCH PLAN

 3軸合成加速度を利用した　

 特徴量として 平均と分散を利用した　

 2人分のモーションキャプチャデータを使用した　

 施設の全員分のデータを利用した　

+++

 3軸合成加速度を利用した　

 -> 合成せずに利用する
  
+++

@snap[midpoint span-48]
 平均と分散を利用した　

 -> 他の特徴量を使う
@snapend

<!--
![image](/fig/Rplot.pdf)
![image](fig/Rplot02.pdf)
-->
+++

 2人分のモーションキャプチャデータを使用した　

 -> データ量を増やす
  
+++

 施設の全員分のデータを利用した　

 -> データ入力量が多い人のみで分析する

---

